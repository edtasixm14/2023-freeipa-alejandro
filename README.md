TFG-ASIX-FreeIPA						           Alejandro Gambín Gómez


![Poster_FreeIPA](Cartell/Poster_FreeIPA_page-0001.jpg)



# Howto FreeIPA
![](images/freeipa_logo.png)


## Índex

[**1. Que és FreeIPA**](#freeipa)

[**2. Administració de FreeIPA**](#administracio)

[**3. Preparació d’instal·lació Màquines**](#instalacio_maquines)

[3.1 Instal·lació de Fedora 32](#Fedora32)

[**4. Preparació de la Xarxa**](#xarxa)

[**5. Procés d’instal·lació del servidor**](#intalacio_servidor)

-[5.1 ](#ip_estatica)[Configuració](#ip_estatica)[ IP estàtica	7](#ip_estatica)

-[5.2 Instal·lació servidor FreeIPA	7](#ordres_freeipa)

[**6. Obrir ports per acceptar connexions**](#ports)

[**7. Instal·lació client FreeIPA**](#instalacio_client)

[**8. Password Vault**](#password_vault)

[**9. Problemes trobats**](#problemes_trobats)

[**10. Conclusions**](#conclusions)
#
#

# <a name="freeipa"></a>**1. Que és FreeIPA**

FreeIPA és una solució integrada de software lliure per autenticació d’identitats Proporciona una plataforma centralitzada per administrar i assegurar la identitat dels usuaris, així com per gestionar les polítiques i auditories relacionades.

**I** Identity

**P** Politics  

**A** Audit  **Free** 

Aquesta eina s'utilitza per a la creació d'un controlador de domini entre màquines Linux, encara que si pot sincronitzar-se amb un AD Active Directory de Windows.

Els usuaris, màquines, serveis i polítiques són configurades en un mateix lloc, amb la mateixa eina, això permet que l'administrador pugui fer les gestions molt més fàcils.


![](images/1.png)




**Directori d’usuaris**

Freeipa inclou un directori **LDAP** (Light Directory Access Protocol) on s'emmagatzema tota la informació dels usuaris, grups i polítiques d'accés. Aquest directori s'utilitza per autenticar els usuaris i proporcionar informació del perfil.


**Servei d’autenticació de Kerberos**

Utilitzem **Kerberos** per autenticar els usuaris i proporcionar tickets d'autenticació. Kerberos és un protocol de xarxa segur que utilitza criptografia per garantir l'autenticitat i confidencialitat de les comunicacions.**


**Gestió de polítiques**

FreeIPA permet als administradors establir polítiques d'accés i autorització pels usuaris i grups. Això inclou l'asignació de rols i permisos.


**Administració de certificats**

Proporciona una autoritat (**CA)** interna que emet i gestiona certificats digitals. Aquests certificats digitals s'utilitzen per garantir l'autenticitat i la seguretat de les comunicacions en entorns en línia.


# <a name="administracio"></a>**2. Administració de FreeIPA**

Dins l'administració FreeIPA podem treballar amb interfície gràfica per la web o mitjançant el terminal (CLI).

L'accés per l'administració amb la web es fa mitjançant el nostre navegador i el nostre nom de domini del servidor web.

**https://freeipa.edt.lab/ipa/ui**

![](images/login.png)

**1. Login Web UI**

L'accés per CLI o línia de comandes és l'eina que farem servir per automatitzar tasques i es dona molta més eficiència a els administradors.


|<p>**[freeipa@client ~]$ kinit admin**</p><p>Password for admin@EDT.LAB:</p><p></p><p>**[freeipa@client ~]$ klist**</p><p>Ticket cache: KCM:1000</p><p>Default principal: admin@EDT.LAB</p><p></p><p>Valid starting   	Expires          	Service principal</p><p>06/13/2023 17:59:29  06/14/2023 17:21:59  krbtgt/EDT.LAB@EDT.LAB</p>|
| :- |



# <a name="instalacio_maquines"></a>**3. Preparació d’instal·lació Màquines**

Hem de fer una preparació prèvia a la nostra instal·lació.

En aquest cas hem fet servir les distribucions de Fedora 32 tant per client com per servidor.

## <a name="Fedora32"></a>**3.1 Instal·lació de Fedora 32**

Amb l'ús del Virtmanager hem realitzat la instal·lació de la nostra màquina virtual.

Procés d’instal·lació:


![](images/instalacio1.png)


![](images/instalacio2.png)

# <a name="xarxa"></a>**4. Preparació de la Xarxa**

Hem configurat el nostre Virt Manager amb una xarxa NAT que té aquest rang de IPs 192.168.10.2 - 192.168.10.254

Amb un domini per la nostra xarxa que s'anomena freeipa.server.edt

![](images/nat.png)

**4.1 NAT configuration**

# <a name="instalacio_servidor"></a>**5. Process d’instal·lació del servidor** 

## <a name="ip_estatica"></a>**5.1 Configuració IP estàtica**
Hem de configurar una IP estàtica pel nostre servidor freeipa, editem el següent fitxer amb els canvis adients.

|***sudo vim /etc/sysconfig/network-scripts/ifcfg-eth0***|
| :- |

Amb el Network Manager es pot fer la configuració d’una IP estàtica molt més fàcilment.

[foto]

## <a name="ordres_freeipa"></a>**5.2 Instal·lació servidor FreeIPA** 

Canviar el nostre hostname:

|<p>[freeipa@freeipa ~]$ vim /etc/hostname</p><p>freeipa.edt.lab</p>|
| :- |

Canviar l'arxiu /etc/resolv.conf 

|<p>[freeipa@freeipa ~]$ cat /etc/resolv.conf</p><p>nameserver 8.8.8.8</p><p>nameserver 8.8.4.4</p><p># Aixo es fa per quan fem l’instal·lació de Freeipa aquests siguin els nostres DNS externs</p>|
| :- |

L’ordre d’instal·lació del nostre servidor FreeIPA

|[freeipa@freeipa ~]$ ipa-server-install -a freeipaDir -p freeipaDir --domain=freeipa.edt.lab --realm=EDT.LAB --hostname freeipa.edt.lab --ip-address=192.168.10.2 --mkhomedir --setup-dns --ssh-trust-dns --forwarder=8.8.8.8 --auto-reverse --no-host-dns -U|
| :- |


![](images/resum_instalacio.png)

**5.1 Resultat d’instal·lació**

# <a name="ports"></a>**6. Obrir ports per acceptar connexions**
Hem d'obrir els següents ports per poder acceptar connexions i que el nostre servidor freeipa funcioni correctament.

![](images/ports.png)

**6.1 Ports a obrir**

Aquestes són les ordres que farem servir:

```
firewall-cmd --zone=public --permanent --add-service=https
firewall-cmd --zone=public --permanent --add-service=http
firewall-cmd --zone=public --permanent --add-service=ldaps
firewall-cmd --zone=públic --permanent --add-service=ldap
firewall-cmd --zone=public --permanent --add-service=kerberos
firewall-cmd --zone=public --permanent --add-port=53/tcp
firewall-cmd --zone=public --permanent --add-port=53/udp
firewall-cmd --zone=public --permanent --add-service=ntp
firewall-cmd --zone=public --permanent --add-port=464/tcp
firewall-cmd --zone=public --permanent --add-port=464/udp
```
```
firewall-cmd --reload
```


# <a name="instalacio_client"></a>**7. Instal·lació client FreeIPA**

Ha de ser a la mateixa xarxa que el nostre servidor i que pugui sortir pel DNS del nostre servidor FreeIPA.

Per fer això hem de configurar el DNS de la nostra màquina client perquè surti per la IP estàtica del nostre servidor (**192.168.10.2**)

Resultat /etc/resolv.conf 

|<p>search edt.lab</p><p>nameserver 192.168.10.2</p>|
| :- |

Comprovació que la resolució de noms sigui correcta

|<p>[freeipa@client ~]$ ping www.google.com</p><p>PING www.google.com (216.58.215.164) 56(84) bytes of data.</p><p>64 bytes from mad41s07-in-f4.1e100.net (216.58.215.164): icmp\_seq=1 ttl=118 time=22.0 ms</p>|
| :- |

Resultat /etc/hosts per resolució de domini 

|<p>127\.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4</p><p>::1     	localhost localhost.localdomain localhost6 localhost6.localdomain6</p><p>192\.168.10.2    freeipa.edt.lab edt.lab</p>|
| :- |

Ordre d’instal·lació del client

|[root@client freeipa]# ipa-client-install --mkhomedir --principal admin --password=freeipaDir|
| :- |

Si tot ha anat bé hauria de trobar el REALM i tota la configuració donada pel servidor FreeIPA.


# <a name="password_vault"></a>**8. Password Vault FreeIPA**
#
<a name="_u6yknhed3q6m"></a><https://www.freeipa.org/page/V4/Password_Vault_2.0>
#
#
# <a name="problemes_trobats"></a>**9. Problemes trobats**
En primera estància volia fer-ho en Debian, però les dependències i paquets no es trobaven a cap lloc.
Vaig fer un canvi a CentOS 7 i dins de la màquina els mirros de CentOS 7 estaven tots caiguts i no podia descarregar cap paquet.

Els problemes de resolució de Noms del meu client cap al meu servidor dns del Freeipa. Vaig haver de reinstal·lar tot el servidor Freeipa i fer canvis dins del domini.
#
#

# <a name="conclusions"></a>**10. Conclusions**
El programari FreeIPA és molt adequat per entorns on es necessita una gestió centralitzada com podria ser la nostra escola.

Es una eina molt sencilla d’utilitzar gràcies a la seva WebUI on pots fer tot tipus de gestions sense necessitat d’un coneixement alt de CLI.


Utilitzar FreeIPA pot ajudar a reduir la complexitat i els costos relacionats amb la gestió d’identitats i l’accés en un entorn empresarial.

FreeIPA és una solució molt potent per a la gestió d’identitats i de l’accés en un entorn de xarxa. Amb les seves funcionalitats avaçades i la seva integració amb altres serveis, proporciona una base sòlida per a la seguretat i la gestió eficient



