# Ports a Obrir per el nostre servidor

```
firewall-cmd --zone=public --permanent --add-service=https
firewall-cmd --zone=public --permanent --add-service=http
firewall-cmd --zone=public --permanent --add-service=ldaps
firewall-cmd --zone=públic --permanent --add-service=ldap
firewall-cmd --zone=public --permanent --add-service=kerberos
firewall-cmd --zone=public --permanent --add-port=53/tcp
firewall-cmd --zone=public --permanent --add-port=53/udp
firewall-cmd --zone=public --permanent --add-service=ntp
firewall-cmd --zone=public --permanent --add-port=464/tcp
firewall-cmd --zone=public --permanent --add-port=464/udp
```



# Recarregar el firewall 
```
firewall-cmd --reload
```
